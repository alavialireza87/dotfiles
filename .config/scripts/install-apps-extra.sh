#! /bin/sh

# Install my personal apps for personal use (extra)
echo " ============== Updating System =============="
pacman -Syu --noconfirm

echo " ============== Installing Extra Packages =============="

pacman -S --noconfirm \
mpv gitui nitrogen lxappearance alacritty bat bitwarden blueman bluez-utils \
pulseaudio-bluetooth dbeaver galculator flatpak remmina transmission-cli \
transmission-qt gufw thunderbird persepolis gcolor3 mousepad neovim mpv \
simplescreenrecorder

read -p "Install Browsers? (y/n)?" choice
case "$choice" in 
  y|Y )
      pacman -S --noconfirm qutebrowser firefox firefox-developer-edition chromium;;
  n|N ) echo "Skipping browsers...";;
  * ) echo "invalid";;
esac

echo "============ FINNISHED! ==========="
