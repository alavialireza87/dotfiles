#!/bin/bash

## this script installs AUR apps and less important apps
echo "======== Setting up YAY ==========="
echo "Checking if yay is installed"
# Check if yay is installed
if ! command -v yay &> /dev/null
then
    echo "yay is not installed. Installing now..."
    sudo pacman -S --needed git base-devel
    git clone https://aur.archlinux.org/yay.git $HOME/yay
    cd $HOME/yay
    makepkg -si --noconfirm
    rm -rf $HOME/yay
else
    echo "yay is already installed."
fi

echo "======== Installing AUR apps ========="
yay -S timeshift clipit pa-applet-git i3exit musikcube-bin
read -p "Install AUR Packages? (y/n)" choice
case "$choice" in
    y|Y )
        yay -S timeshift clipit pa-applet-git i3exit musikcube-bin cava sioyek \
            vorta vscodium-bin xkblayout-state-git;;
    n|N ) echo "Skipping AUR Packages...";;
    * ) echo "invalid";;
esac

echo "=========== FINNISHED! ============"
