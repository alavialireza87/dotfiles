local lsp = require("lsp-zero")

lsp.preset("recommended")

lsp.ensure_installed({
    'lua_ls',
    'eslint',
    'rust_analyzer',
    'volar'
})

-- Fix Undefined global 'vim'
lsp.configure('lua_ls', {
    settings = {
        Lua = {
            diagnostics = {
                globals = { 'vim' }
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
                checkThirdParty = false,
            },
        }
    }
})

local cmp = require('cmp')
local cmp_select = { behavior = cmp.SelectBehavior.Select }
local cmp_mappings = lsp.defaults.cmp_mappings({
    ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
    ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
    ['<CR>'] = cmp.mapping.confirm(),
    ["<C-Space>"] = cmp.mapping.complete(),
})

cmp_mappings['<Tab>'] = nil
cmp_mappings['<S-Tab>'] = nil

lsp.setup_nvim_cmp({
    mapping = cmp_mappings
})

lsp.set_preferences({
    suggest_lsp_servers = false,
    sign_icons = {
        error = 'E',
        warn = 'W',
        hint = 'H',
        info = 'I'
    }
})

-- this block: only add this keymaps for buffers that have an LSP associated with them
lsp.on_attach(function(client, bufnr)
    local opts = { buffer = bufnr, remap = false }

    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts, { desc = "Go to definition" })
    vim.keymap.set("n", "K", vim.lsp.buf.hover, opts, { desc = "Show hover information" })
    vim.keymap.set("n", "<leader>vws", vim.lsp.buf.workspace_symbol, opts, { desc = "List all workspace symbols" })
    vim.keymap.set("n", "<leader>vd", vim.diagnostic.open_float, opts, { desc = "Show floating diagnostics" })
    vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts, { desc = "Diagnostic goto_next" })
    vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts, { desc = "Diagnostic goto_prev" })
    vim.keymap.set("n", "<leader>vca", vim.lsp.buf.code_action, opts, { desc = "Code action" })
    vim.keymap.set("n", "<leader>vrr", vim.lsp.buf.references, opts, { desc = "References in buffer" })
    vim.keymap.set("n", "<leader>vrn", vim.lsp.buf.rename, opts, { desc = "Rename in buffer" })
    vim.keymap.set("i", "<C-h>", vim.lsp.buf.signature_help, opts, { desc = "signature_help" })
    vim.keymap.set("n", "<leader>f", vim.lsp.buf.format, { desc = "Format buffer" })
end)

lsp.setup()

vim.diagnostic.config({
    virtual_text = true
})
