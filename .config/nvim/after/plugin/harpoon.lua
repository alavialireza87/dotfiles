local mark = require("harpoon.mark")
local ui = require("harpoon.ui")

vim.keymap.set("n", "<leader>a", mark.add_file, { desc = "Add file to harpoon" })
vim.keymap.set("n", "<leader>e", ui.toggle_quick_menu, { desc = "Toggle Harpoon Menu" })

vim.keymap.set("n", "<C-p>", function() ui.nav_prev() end)
vim.keymap.set("n", "<C-n>", function() ui.nav_next() end)
