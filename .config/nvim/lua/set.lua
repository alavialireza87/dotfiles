--------------------------
--      GLOBAL          --
--------------------------

vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hlsearch = false
vim.opt.incsearch = true

-- vim.opt.clipboard="unnamedplus"

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

--------------------------
--      IF VSCODE       --
--------------------------
if vim.g.vscode then
    -- to underline selected text with vim-sandwich
    vim.cmd([[
    highlight OperatorSandwichBuns guifg='#aa91a0' gui=underline ctermfg=172 cterm=underline
    highlight OperatorSandwichChange guifg='#edc41f' gui=underline ctermfg='yellow' cterm=underline
    highlight OperatorSandwichAdd guibg='#b1fa87' gui=none ctermbg='green' cterm=none
    highlight OperatorSandwichDelete guibg='#cf5963' gui=none ctermbg='red' cterm=none
    ]])
    vim.opt.clipboard = "unnamedplus"
    vim.opt.termguicolors = true
else
    --------------------------
    --      IF NEOVIM       --
    --------------------------
    vim.opt.number = true
    vim.opt.relativenumber = true
    vim.opt.cursorline = true
    vim.opt.termguicolors = true

    -- TABS
    vim.opt.tabstop = 4
    vim.opt.softtabstop = 4
    vim.opt.shiftwidth = 4
    vim.opt.expandtab = true

    vim.opt.smartindent = true

    vim.opt.wrap = false

    vim.opt.scrolloff = 8
    vim.opt.signcolumn = "yes"
    vim.opt.isfname:append("@-@")
    vim.opt.updatetime = 50

    vim.g.netrw_browse_split = 0
    -- vim.g.netrw_banner = 0
    vim.g.netrw_winsize = 25

    vim.opt.colorcolumn = "80"

    -- set color scheme
    -- vim.cmd.colorscheme "catppuccin"

    -- run colorscheme after all the setup of vim and its configs has been run
    -- because theme gets put in opt and will be run before :packadd if not done this way
    vim.cmd("autocmd VimEnter * ++once colorscheme catppuccin");
end
